<?php
namespace MU_Plugins\Post_Types;

/**
 * Plugin Name:       Post Types
 * Description:       Registers our custom post types.
 * Version:           1
 * Author:            Keiron Lowe
 * Author URI:        https://theindividualagency.com
 */
class Post_Types {


    /**
     * Registers our custom post types.
     *
     * Dashicons reference can be found here: https://developer.wordpress.org/resource/dashicons/
     */
    public function __construct() {
        require_once 'class-custom-post-type.php';

        // Products.
        // new Custom_Post_Type( 'singular', 'plural' );
    }
}

new Post_Types();
