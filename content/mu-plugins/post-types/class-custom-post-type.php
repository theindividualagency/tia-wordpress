<?php
namespace MU_Plugins\Post_Types;

/**
 * Helper class for generating custom post types.
 */
class Custom_Post_Type {


    /**
     * Our singular name for this taxonomy.
     *
     * @var string
     */
    private $singular = '';


    /**
     * Our plural name for this taxonomy.
     *
     * @var string
     */
    private $plural = '';


    /**
     * Our options for this taxonomy.
     *
     * @var array
     */
    private $options = array();


    /**
     * Takes the provided singular and plural names to generate the labels and
     * the arguments to generate a custom post type.
     *
     * @param string $singular The singular name for this post type.
     * @param string $plural The plural name for this post type.
     * @param array  $options Our options for this post type.
     */
    public function __construct( string $singular, string $plural, array $options = array() ) {
        $this->singular = $singular;
        $this->plural   = $plural;
        $this->options  = $this->get_options( $options );

        add_action( 'init', array( $this, 'register' ) );
    }


    /**
     * Registers our custom post type.
     *
     * @return void
     */
    public function register(): void {
        register_post_type( sanitize_title( $this->singular ), $this->options );
    }


    /**
     * Returns our merged $options property.
     *
     * @param array $options Our option overrides.
     * @return array
     */
    public function get_options( array $options ) : array {
        // phpcs:disable WordPress.WP.I18n.NonSingularStringLiteralText

        // Our labels.
        $labels = array(
            'name'               => _x( $this->plural, 'post type general name' ),
            'singular_name'      => _x( $this->singular, 'post type singular name' ),
            'add_new'            => _x( 'Add New', 'book' ),
            'add_new_item'       => __( 'Add New ' . $this->singular ),
            'edit_item'          => __( 'Edit ' . $this->singular ),
            'new_item'           => __( 'New ' . $this->singular ),
            'all_items'          => __( 'All ' . $this->plural ),
            'view_item'          => __( 'View ' . $this->singular ),
            'search_items'       => __( 'Search ' . $this->plural ),
            'not_found'          => __( 'No ' . $this->plural . ' found' ),
            'not_found_in_trash' => __( 'No ' . $this->plural . ' found in the Trash' ),
            'parent_item_colon'  => '',
            'menu_name'          => $this->plural,
        );

        // Set our options property.
        $defaults = array(
            'labels'      => $labels,
            'description' => 'Holds our ' . $this->plural . ' and ' . $this->singular . ' specific data',
            'public'      => true,
            'supports'    => array( 'title', 'editor' ),
            'has_archive' => false,
        );

        return $this->extend( $defaults, $options );
    }


    /**
     * Merges two arrays, overwriting the values in $base with the values in
     * $replacements if they exist and returning this new array
     *
     * @param array $base The base array values.
     * @param array $replacements The override array values.
     * @return array
     */
    public function extend( array $base = array(), array $replacements = array() ) : array {
        $base         = ! is_array( $base ) ? array() : $base;
        $replacements = ! is_array( $replacements ) ? array() : $replacements;
        return array_replace_recursive( $base, $replacements );
    }
}
