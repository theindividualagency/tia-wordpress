<?php
namespace MU_Plugins\Taxonomies;

/**
 * Plugin Name:       Taxonomites
 * Description:       Registers our custom taxonomies.
 * Version:           1
 * Author:            Keiron Lowe
 * Author URI:        https://theindividualagency.com
 */
class Taxonomies {


    /**
     * Generates the post types
     */
    public function __construct() {
        require_once 'class-taxonomy.php';

        // Collections.
        // new Taxonomy( 'Singular', 'Plural' );
    }
}

new Taxonomies();
