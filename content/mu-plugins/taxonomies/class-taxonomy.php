<?php
namespace MU_Plugins\Taxonomies;

/**
 * Taxonomies Generator
 */
class Taxonomy {


    /**
     * Our singular name for this taxonomy.
     *
     * @var string
     */
    private $singular = '';


    /**
     * Our plural name for this taxonomy.
     *
     * @var string
     */
    private $plural = '';


    /**
     * The post types to attach this taxonomy to.
     *
     * @var array
     */
    private $post_types = array();


    /**
     * Our options for this taxonomy.
     *
     * @var array
     */
    private $options = array();


    /**
     * Takes the provided singular and plural names to generate the labels and
     * the arguments to generate a custom taxonomy for the $post_types
     *
     * @param string $singular The singular name of the taxonomy.
     * @param string $plural The plural name of the taxonomy.
     * @param array  $post_types The post types to attach taxonomies to.
     * @param array  $options Array of options we want to overwrite.
     */
    public function __construct( string $singular, string $plural, array $post_types, array $options = array() ) {
        $this->singular   = $singular;
        $this->plural     = $plural;
        $this->post_types = $post_types;
        $this->options    = $this->get_options( $options );

        add_action( 'init', array( $this, 'register' ) );
    }


    /**
     * Registers our custom taxonomy.
     *
     * @return void
     */
    public function register(): void {
        register_taxonomy( sanitize_title( $this->singular ), $this->post_types, $this->options );
    }


    /**
     * Returns our merged $options property.
     *
     * @param array $options Our option overrides.
     * @return array
     */
    public function get_options( array $options ) : array {
        // phpcs:disable WordPress.WP.I18n.NonSingularStringLiteralText

        // Our labels.
        $labels = array(
            'name'              => _x( $this->plural, 'taxonomy general name' ),
            'singular_name'     => _x( $this->singular, 'taxonomy singular name' ),
            'search_items'      => __( 'Search ' . $this->plural ),
            'all_items'         => __( 'All ' . $this->plural ),
            'parent_item'       => __( 'Parent ' . $this->singular ),
            'parent_item_colon' => __( 'Parent ' . $this->singular . ':' ),
            'edit_item'         => __( 'Edit ' . $this->singular ),
            'update_item'       => __( 'Update ' . $this->singular ),
            'add_new_item'      => __( 'Add New ' . $this->singular ),
            'new_item_name'     => __( 'New ' . $this->singular . ' Name' ),
            'menu_name'         => __( $this->plural ),
        );

        // Set our options property.
        $defaults = array(
            'hierarchical'      => true,
            'labels'            => $labels,
            'show_ui'           => true,
            'show_admin_column' => true,
            'query_var'         => true,
            'rewrite'           => array( 'slug' => $this->singular ),
        );

        return $this->extend( $defaults, $options );
    }


    /**
     * Merges two arrays, overwriting the values in $base with the values in
     * $replacements if they exist and returning this new array
     *
     * @param array $base The base array values.
     * @param array $replacements The override array values.
     * @return array
     */
    public function extend( array $base = array(), array $replacements = array() ) : array {
        $base         = ! is_array( $base ) ? array() : $base;
        $replacements = ! is_array( $replacements ) ? array() : $replacements;
        return array_replace_recursive( $base, $replacements );
    }
}
