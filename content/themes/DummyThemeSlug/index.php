<?php

/**
 * Underpin uses blade templates to render the page content, so to seperate
 * our logic from our presentation. The blade templates can be found within the
 * views folder, and these templates and their corresponding data is loaded from
 * the includes/pages/ folder.
 */
do_action( 'underpin_content' );
