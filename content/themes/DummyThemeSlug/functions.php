<?php

/**
 * Initialization
 *
 * The first thing we will do is create a new Underpin instance. This
 * sets up the framework and connects all the various parts.
 */
$app            = Underpin\Underpin::get_instance();
$app->namespace = THEME_NAMESPACE;


/**
 * Bootstrap
 *
 * This is our opportunity to tweak any of the options for our bootstrap.
 * Refer to the documentation for the options and don't forget to return
 * the instance of $bootstrap!
 */
$app->bootstrap(function( Underpin\Bootstrap\Bootstrap $bootstrap ) {
    return $bootstrap;
});


/**
 * Setup
 *
 * This is where we can modify the base theme setup, such as adding our
 * scripts/styles, images sizes etc.
 */
$app->config(function( Underpin\Config\Config $config ) {
    return $config;
});


/**
 * Render
 *
 * This is where we load the view for the current page.
 */
$app->loader(function( Underpin\View\Loader $loader ) {

    // Render the view.
    $loader->set_view( $loader->get_view() );
    $loader->render();

    return $loader;
});
