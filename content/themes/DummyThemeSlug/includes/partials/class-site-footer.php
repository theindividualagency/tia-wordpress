<?php
namespace DummyNamespace\Partials;

use Underpin\Partial\Partial;

/**
 * This class handles the logic for our site footer.
 *
 * @package DummyNamespace
 */
class Site_Footer extends Partial {


    /**
     * Returns an array containing the data for our page.
     *
     * @return array
     */
    public static function data(): object {
        return (object) array(
            'copyright' => 'Copyright ' . date( 'Y' ),
        );
    }
}
