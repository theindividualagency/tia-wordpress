<?php
namespace DummyNamespace\Partials;

use Underpin\Partial\Partial;

/**
 * This class handles the logic for our site sprite.
 *
 * @package DummyNamespace
 */
class Site_Sprite extends Partial {


    /**
     * Returns an array containing the data for our page.
     *
     * @return array
     */
    public static function data(): array {
        return \Underpin\Helpers\Sprite::get_all();
    }
}
