<?php
namespace DummyNamespace\Pages;

use Underpin\Views\Page;

/**
 * This class is acts as the default view if another more specific one is not
 * available.
 *
 * @package DummyNamespace
 */
class Index extends Page {


    /**
     * Returns an array containing the data for our page.
     *
     * @return array
     */
    public function data(): array {
        return array(
            'title' => 'Default',
        );
    }
}
