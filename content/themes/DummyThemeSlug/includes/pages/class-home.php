<?php
namespace DummyNamespace\Pages;

use Underpin\Views\Page;

/**
 * Handles the view for our about page.
 *
 * @package DummyNamespace
 */
class Home extends Page {


    /**
     * Returns an array containing the data for our page.
     *
     * @return array
     */
    public function data(): array {
        return array(
            'title' => 'Home',
        );
    }


    /**
     * Returns true if this class should be executed on this request.
     *
     * @return boolean
     */
    public static function should_execute(): bool {
        return is_front_page();
    }
}
