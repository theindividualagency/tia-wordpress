<?php
namespace DummyNamespace\Pages;

use Underpin\Views\Page;

/**
 * Handles the view for our about page.
 *
 * @package DummyNamespace
 */
class About extends Page {


    /**
     * Contains the ID for this page.
     *
     * @var int
     */
    protected static $id = 5;


    /**
     * Returns an array containing the data for our page.
     *
     * @return array
     */
    public function data(): array {
        return array(
            'title' => 'About',
        );
    }
}
