<?php
namespace DummyNamespace\Pages;

use Underpin\Views\Archive;

/**
 * This class acts as the view for the products page.
 *
 * @package DummyNamespace
 */
class Products extends Archive {


    /**
     * Contains the slug for the post type.
     *
     * @var int
     */
    protected static $post_type_slug = 'product';


    /**
     * Returns an array containing the data for our page.
     *
     * @return array
     */
    public function data(): array {
        return array(
            'title' => 'Products',
        );
    }
}
