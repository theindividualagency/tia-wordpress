<?php
namespace DummyNamespace\Post_Types;

use Underpin\Components\Post_Type\Post_Type;

/**
 * Handles the view for our about page.
 *
 * @package DummyNamespace
 */
class Product extends Post_Type {


    /**
     * Contains the slug for this post type.
     *
     * @var string
     */
    protected static $post_type_slug = 'product';
}
