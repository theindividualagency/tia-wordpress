{{-- This is the template for the site header --}}
@inject ( 'site_header', '\DummyNamespace\Partials\Site_Header' )

<header class="c-site-header">
    <p>{{ $site_header->title }}</p>
</header>
