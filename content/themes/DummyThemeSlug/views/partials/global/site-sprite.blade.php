{{-- This is the template for the site sprite --}}
@inject ( 'sprites', '\DummyNamespace\Partials\Site_Sprite' )

<svg width="0" height="0" class="u-hidden-visually">

    @foreach( $sprites as $sprite )
        @include ( 'sprite.' . $sprite )
    @endforeach

</svg>
