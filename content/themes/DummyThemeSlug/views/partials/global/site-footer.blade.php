{{-- This is the template for the site footer --}}
@inject ( 'site_footer', '\DummyNamespace\Partials\Site_Footer' )

<footer class="c-site-footer">
    <p>{{ $site_footer->copyright }}</p>
</footer>
