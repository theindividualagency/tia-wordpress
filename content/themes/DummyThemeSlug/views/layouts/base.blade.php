<!doctype html>
<html {{ language_attributes() }} class="no-js {{ apply_filters( 'html_class', '' ) }}">
    <head>
        <meta charset="{{ bloginfo( 'charset' ) }}">
        <meta name="theme-color" content="#000000">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        {{ wp_head() }}
    </head>
    <body {{ body_class() }}>

        {{-- Site Header --}}
        @if ( apply_filters( 'DummyNamespace_include_site_header', true ) )
            @include ( 'partials.global.site-header' )
        @endif

        {{-- Site Nav --}}
        @include ( 'partials.global.site-nav' )


        {{-- Main Page Content --}}
        @yield('content')


        {{-- Site Footer --}}
        @if ( apply_filters( 'Dummynamespace_include_site_footer', true ) )
            @include ( 'partials.global.site-footer' )
        @endif

        {{-- Site Sprite --}}
        @include ( 'partials.global.site-sprite' )

        {{ wp_footer() }}

    </body>
</html>
