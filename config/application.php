<?php
use Dotenv\Dotenv;

/**
 * Your main configuration goes in this file, while environment specific configuration goes into their respective file
 * in config/environments/{{ WP_ENV }}.php.
 *
 * Any configuration in this file will be applied to all environments.
 */
$dotenv = ( DotEnv::create( dirname( __DIR__ ) ) );
$dotenv->load();
$dotenv->required([
    'DB_NAME',
    'DB_USER',
    'DB_PASSWORD',
]);

// Setup our default global environment constant.
define( 'WP_ENV', getenv( 'WP_ENV') ?: 'production' );

// Salts.
define( 'AUTH_KEY', getenv( 'AUTH_KEY' ) );
define( 'SECURE_AUTH_KEY', getenv( 'SECURE_AUTH_KEY' ) );
define( 'LOGGED_IN_KEY', getenv( 'LOGGED_IN_KEY' ) );
define( 'NONCE_KEY', getenv( 'NONCE_KEY' ) );
define( 'AUTH_SALT', getenv( 'AUTH_SALT' ) );
define( 'SECURE_AUTH_SALT', getenv( 'SECURE_AUTH_SALT' ) );
define( 'LOGGED_IN_SALT', getenv( 'LOGGED_IN_SALT' ) );
define( 'NONCE_SALT', getenv( 'NONCE_SALT' ) );

// Database settings.
define( 'DB_NAME', getenv( 'DB_NAME' ) );
define( 'DB_USER', getenv( 'DB_USER' ) );
define( 'DB_PASSWORD', getenv( 'DB_PASSWORD' ) );
define( 'DB_HOST', getenv( 'DB_HOST' ) ?: '127.0.0.1' );
define( 'DB_CHARSET', 'utf8mb4' );
define( 'DB_COLLATE', '' );
define( 'DB_PREFIX', getenv( 'DB_PREFIX' ) ?: 'wp_' );
$table_prefix = DB_PREFIX; // phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited

// Security.
define( 'FS_METHOD', 'direct' );
define( 'FORCE_SSL_LOGIN', true );
define( 'DISALLOW_FILE_EDIT', true );

// Performance.
define( 'WP_POST_REVISIONS', 3 );
define( 'AUTOSAVE_INTERVAL', 240 );
define( 'COMPRESS_CSS', true );
define( 'COMPRESS_SCRIPTS', true );
define( 'CONCATENATE_SCRIPTS', true );
define( 'ENFORCE_GZIP', true );

/**
 * Load our environment configuration.
 * Any consants set after this could depend on the environment.
 */
$env_config = __DIR__ . '/environments/' . WP_ENV . '.php';
if ( file_exists( $env_config ) ) {
    require_once $env_config;
}

// Theme Settings.
define( 'THEME_NAME', 'DummyThemeSlug' );
define( 'THEME_NAMESPACE', 'DummyNamespace' );

// Set our base URL's and directories.
define( 'DOC_ROOT', dirname( __DIR__ ) );
define( 'WP_SITEURL', WP_HOME . '/wp' );
define( 'WP_CONTENT_DIR', dirname( __DIR__ ) . '/content' );
define( 'WP_THEME_DIR', dirname( __DIR__ ) . '/content/themes/' . THEME_NAME );
define( 'WP_CONTENT_URL', WP_HOME . '/content' );

// Blade.
define( 'BLADE_CACHE', WP_CONTENT_DIR . '/blade_cache' );
define( 'BLADE_VIEWS', WP_THEME_DIR . '/views' );

// Debugging.
define( 'WP_DISABLE_FATAL_ERROR_HANDLER', true );
if ( WP_DEBUG ) {
    define( 'WP_DEBUG_LOG', true );
    define( 'WP_DEBUG_DISPLAY', true );
}

// Bootstrap WordPress.
if ( ! defined( 'ABSPATH' ) ) {
    define( 'ABSPATH', dirname( __DIR__ ) . '/wp/' );
}
