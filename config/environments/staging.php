<?php

/**
 * These are the configuration variables for our staging environment.
 */
define( 'WP_HOME', 'DummyStagingDomain' );
define( 'WP_DEBUG', true );
